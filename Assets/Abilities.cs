﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abilities : MonoBehaviour {

	public static Abilities instance = null; 
	public bool leftAbility1 = false;
	public bool leftAbility2 = false;
	public bool leftAbility3 = false;
	public bool rightAbility1 = false;
	public bool rightAbility2 = false;
	public bool rightAbility3 = false;
	public string leftAbilityList = "";
	public string rightAbilityList = "";

	void Awake() {
		if(instance == null) {
			instance = this;
		} else if(instance != this) {
			Destroy(this.gameObject);
		}
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void AbilityList() {
		string temp = "";
		string temp2 = "";


		if (leftAbility1) {
			temp += "1,";
		}
		if (leftAbility2) {
			temp += "2,";
		}
		if (leftAbility3) {
			temp += "3";
		}
		if (rightAbility1) {
			temp2 += "1,";
		}
		if (rightAbility2) {
			temp2 += "2,";
		}
		if (rightAbility3) {
			temp2 += "3";
		}

		leftAbilityList = temp;
		rightAbilityList = temp2;
	}
		


	public void ApplyAbility(int side, int ability) {
		if (side == 1) {
			switch (ability) {
			case 1:
				leftAbility1 = true;
				break;
			case 2:
				leftAbility2 = true;
				break;
			case 3:
				leftAbility3 = true;
				break;
			}
		} else if (side == 2) {
			switch (ability) {
			case 1:
				rightAbility1 = true;
				break;
			case 2:
				rightAbility2 = true;
				break;
			case 3:
				rightAbility3 = true;
				break;
			}
				
		}
		AbilityList ();
		AbilityUpdateUI.instance.UpdateUI ();
	}
}
