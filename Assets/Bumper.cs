﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour {

	public bool isBump1;
	public static float bump1Speed = 5f;
	public static float bump2Speed = 5f;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (isBump1) {
			transform.Translate (0f, Input.GetAxis ("Vertical") * bump1Speed * Time.deltaTime, 0f);
			Debug.Log("bump1 speed: " +bump1Speed);
		} else {
			transform.Translate (0f, Input.GetAxis ("Vertical2") * bump2Speed * Time.deltaTime, 0f);
			Debug.Log("bump2 speed: " +bump2Speed);
		}
	}
}
